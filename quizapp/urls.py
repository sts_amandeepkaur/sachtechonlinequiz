from django.urls import path
from quizapp import views

app_name="quizapp"

urlpatterns = [
    path('',views.signup,name="register"),
    path('uslogout',views.uslogout,name="uslogout"),    
    path('dashboard',views.dashboard,name="dashboard"),    
    path('quiz',views.quiz,name="quiz"),    
]
