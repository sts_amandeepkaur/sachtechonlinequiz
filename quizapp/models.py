from django.db import models
from django.contrib.auth.models import User
class category(models.Model):
    c_name = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    image = models.ImageField(upload_to = "categories/%Y/%m/%d")
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.c_name

class add_question(models.Model):
    question = models.TextField()
    q_category = models.ForeignKey(category,on_delete=models.CASCADE,related_name="questions")
    options = models.TextField()
    correct_option = models.CharField(max_length=300)
    description = models.TextField(blank=True)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.question

class add_college(models.Model):
    college_name = models.CharField(max_length=500)
    landmark = models.CharField(max_length=250)
    state = models.CharField(max_length=250,blank=True)
    pin_code = models.IntegerField(blank=True)
    contact_number = models.IntegerField(blank=True)
    email = models.EmailField(blank=True)
    website = models.URLField(blank=True)
    added_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.college_name

class register(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    college = models.ForeignKey(add_college,on_delete=models.CASCADE)
    contact_number = models.IntegerField()
    father_name = models.CharField(max_length=250)
    mother_name = models.CharField(max_length=250)
    date_of_birth = models.DateField()
    admission_no = models.IntegerField(blank=True)
    roll_no = models.IntegerField()
    registred_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username