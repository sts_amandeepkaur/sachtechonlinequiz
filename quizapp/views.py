from django.shortcuts import render
from django.contrib.auth.models import User
from .models import add_college,register, category,add_question
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, HttpResponseRedirect

all_colleges = add_college.objects.all().order_by('college_name')
all_cat = category.objects.all().order_by('c_name')

def index(request):  
    data={}
    if 'signin' in request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect('/quizapp/dashboard')
        else:
            data['error'] = "Invalid Username or Password"
            data['color'] = "alert alert-danger"
    return render(request,'index.html',data)

def signup(request):
    data = {
        'colleges':all_colleges,
    }
    if request.method=="POST":
        name = request.POST['name']
        fname = request.POST['fname']
        mname = request.POST['mname']
        dob = request.POST['dob']
        contact = request.POST['contact']
        email = request.POST['email']
        pas = request.POST['pas']
        college_id = request.POST['college']
        add_no = request.POST['addmission_no']
        roll_no = request.POST['roll_no']
        check = User.objects.filter(username=email)
        if len(check)==0:
            usr = User.objects.create_user(email,email,pas)
            usr.first_name=name
            usr.save()
            clz = add_college.objects.get(id=college_id)
            rg = register(user=usr,college=clz,contact_number=contact,father_name=fname,mother_name=mname,date_of_birth=dob,admission_no=add_no,roll_no=roll_no)
            rg.save()
            data['success'] = "Registred Successfully!!!"
            data['color'] = 'alert alert-success'
        else:
            data['error'] = "A user with this Email ID already exists!!!"
            data['color'] = 'alert alert-danger'
        return render(request,'register.html',data)
    return render(request,'register.html',data)

def dashboard(request):
    return render(request,'dashboard.html',{'cat':all_cat})

def uslogout(request):
    logout(request)
    return HttpResponseRedirect('/')

def quiz(r):
    all=[]
    c=1
    if r.method=="POST":
        print(r.POST)
    if 'q' in r.GET:
        id = r.GET['q']
        ques = add_question.objects.filter(q_category__id=id)
        for i in ques:
            all_ques={}
            all_ques['sr']='Q.'+str(c)
            all_ques['id']=i.id
            all_ques['question']=i.question
            all_ques['options']=i.options.split('__')
            all_ques['description']=i.description
            all_ques['correct_option']=i.correct_option
            all.append(all_ques)
            c+=1
        return render(r,'quiz.html',{'ques':all})
    return render(r,'quiz.html')