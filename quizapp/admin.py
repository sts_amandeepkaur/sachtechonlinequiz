from django.contrib import admin
from quizapp.models import category, add_question, register, add_college

class categoryAdmin(admin.ModelAdmin):
    list_display = ['id','c_name','description','added_on']

class add_questionAdmin(admin.ModelAdmin):
    list_display = ['id','question','q_category','correct_option','description','added_on']

class add_collegeAdmin(admin.ModelAdmin):
    list_display=['id','college_name','landmark','state','contact_number','email','website','added_on']

admin.site.register(register)
admin.site.register(category,categoryAdmin)
admin.site.register(add_question,add_questionAdmin)
admin.site.register(add_college,add_collegeAdmin)